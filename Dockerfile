FROM node:10

USER root

# Create app directory
WORKDIR /usr/src/app
COPY . .

WORKDIR /usr/src/app/rss_thing
RUN npm install
EXPOSE 8080

WORKDIR /usr/src/app/ben_el
RUN npm install
CMD [ "node", "index.js" ]

# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source


