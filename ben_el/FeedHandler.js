const cp = require("child_process")

const { getArticles } = require("./Utils")

exports.FeedHandler = class FeedHandler {
    constructor(rsser_path) {
        //"../rss_thing/ServerMgr"
        this.rsser = cp.fork(rsser_path)
        this.read = {}
        this.callback = null

        this.rsser.on("message", ({article}) => {
            if(this.callback) this.callback(article)
        })
    }

    getNewArticles() {
        return new Promise((resolve, reject) => {
            getArticles()
            .then(res => {
                resolve(res
                    .filter(article => !this.read[article.link])
                    .map(article => article)
                )
            })
            .catch(reject)
        })
    }

    startAutoFeed(callback){

        this.callback = callback

        this.getNewArticles()
        .then(articles => {
            articles.forEach(article => {
                if(this.callback) this.callback(article)
            })
        })
        .catch(() => null)
    
        this.rsser.send({
            command: "start-auto"
        })
    }

    stopAutoFeed(){

        this.callback = null

        this.rsser.send({
            command: "stop-auto"
        })
    }

    flushFeed(){
        this.getNewArticles()
        .then(articles => {
            articles.forEach(article => {
                //this.handleArticle(article)
                this.read[article.link] = true
            })
        })
    }

    handleArticle(article) {
        this.read[article.link] = true
        return article.link
    }

    pause(){
        this.rsser.send({command: "pause"})
    }
    resume(){
        this.rsser.send({command: "resume"})
    }
}