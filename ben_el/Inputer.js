exports.Inputer = class Inputer {
    constructor(bot) {
        this.bot = bot
        this.callback = null

        this.startListener()
    }

    startListener(){
        this.bot.on("message", (ctx) => {
            if(this.callback){
                this.callback(ctx.message)
                this.callback = null
            }
        })
    }
    stopListener(){
        this.bot.on("message", () => null)
    }



    getNextInput(ctx, ask = null) {
        if (ask) ctx.reply(ask)

        return new Promise((resolve, reject) => {
            this.callback = (msg) => {
                resolve(msg.text)
            }
        })
    }
}