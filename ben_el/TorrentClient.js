let cp = require("child_process")


exports.TorrentClient = class TorrentClient{
    constructor(path){
        this.path = path
        this.proc = cp.fork("../movie_downloader/index")

        this.done_callback = null
        this.info_callback = null

        this.proc.on("message", (msg) => {
            if(msg.info){
                if(this.info_callback) this.info_callback(msg.info)
                this.info_callback = null
            }
            
            else if(msg.done){
                if(this.done_callback) this.done_callback(msg.done)
                this.done_callback = null
            }
        })
    }

    download(torrent, slug){
        this.proc.send({torrent, slug: `${this.path}/${slug}`})
    }

    destroy(){
        this.proc.send({exit: true})
    }

    getInfo(){
        return new Promise((resolve, reject) => {

            this.info_callback = (info) => {
                resolve(info)
            }

            this.proc.send({info: true})
        })
    }
}