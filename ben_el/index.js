
const { myip } = require("./Utils")
const { FeedHandler } = require("./FeedHandler")
const { Inputer } = require("./Inputer")

const { Yts } = require("../movie_downloader/YtsAPI")
const yifysubtitles = require('yifysubtitles');

const { TorrentClient } = require("./TorrentClient")

const si = require('systeminformation');
const Telegraf = require('telegraf')

let bot = new Telegraf('808690938:AAGHjjZ2MogyN0tA-s3KxBOldOxvQSY4qhg')
let Feed = new FeedHandler("../rss_thing/ServerMgr")
let client = new TorrentClient("../movies")



function handleFeed(ctx) {
    Feed.getNewArticles()
        .then(articles => {
            articles.forEach(article => {
                ctx.reply(Feed.handleArticle(article))
            })
        })
        .catch((e) => ctx.reply(e))
}

function feedSize(ctx) {
    Feed.getNewArticles()
        .then(articles => {
            ctx.reply(articles.length)
        })
        .catch((e) => ctx.reply(e))
}

function autoFeed(ctx) {
    Feed.startAutoFeed(article => {
        ctx.reply(Feed.handleArticle(article))
    })
}

function stopAutoFeed(ctx) {
    Feed.stopAutoFeed()
}

function mesureTemp(ctx) {
    si.cpuTemperature().then(temp => {
        console.log(temp)
        ctx.reply(temp.main)
    }).catch(e => console.log(e))
}

function ignoreFeed(ctx) {
    Feed.flushFeed()
}

function listCommands(ctx) {
    Object.keys(commands).map((command) => {
        ctx.reply(command)
    })
}

function getRam(ctx){
    si.mem(mem => {
        ctx.reply(`free: ${mem.free / (1000 * 1000 * 1000)} GB, swap free: ${mem.swapfree / (1000 * 1000 * 1000)} GB`)
    })
}

function stopFeed(ctx){
    Feed.pause()
}

function resumeFeed(ctx){
    Feed.resume()
}

function getMovie(ctx) {
    inputer.getNextInput(ctx, "what movie to search").then(query => {
        console.log("movbie", query)

        new Yts().search(query).then(movies => {
            movies.forEach((movie, i) => {
                ctx.reply(`${i}: ${movie.title}`)
            })

            inputer.getNextInput(ctx, "choose by index").then(movie_index => {
                let movie = movies[movie_index]
                if (!movie) {
                    ctx.reply("no movie in that index")
                    return
                }

                movie.torrents.forEach((torrent, i) => {
                    ctx.reply(`${i}: ${torrent.size}`)
                })

                inputer.getNextInput(ctx, "choose torrent by index").then(torrent_index => {
                    let torrent = movie.torrents[torrent_index]
                    if (!torrent) {
                        ctx.reply("no torrent in that index")
                        return
                    }

                    client.download(torrent.url, movie.slug)

                    yifysubtitles(movie.imdb_code, {
                        path: `../movies/${movie.slug}`,
                        langs: ['he', "en"],
                        format: ['srt', 'vtt'],
                    })
                })
            })
        })
        .catch((e) => ctx.reply("error - " + e))
    })
}

function movies_info(ctx){
    client.getInfo().then(info => {
        info.forEach(movie => {
            ctx.reply(`${movie.url}: ${movie.progress}`)
        })
    })
}


const commands = {

    help: { func: listCommands, description: "list commands" },

    myip: {
        func: (ctx) => {
            myip()
                .then(ip => ctx.reply(ip))
                .catch(e => ctx.reply(e))
        }, description: "get my ip"
    },

    feed: { func: handleFeed, description: "get the whole feed" },
    feed_size: { func: feedSize, description: "get the feed size" },
    feed_ignore: { func: ignoreFeed, description: "flush the feed" },
    feed_pause: {func: stopFeed, description: "puses the feed parsing"},
    feed_resume: {func: resumeFeed, description: "resumes the feed parsing"},

    start_auto: { func: autoFeed, description: "starts auto feed messages" },
    stop_auto: { func: stopAutoFeed, description: "stops auto feed messages" },

    temp: { func: mesureTemp, description: "get temperature (-1 if in container ): )" },
    ram: {func: getRam, description: "gets memory info"},

    movies_info: {func: movies_info, description: "gets info of all downloads"},
    get_movie: { func: getMovie, description: "download a movie" }
}

bot.start((ctx) => ctx.reply('Welcome'))

Object.entries(commands).map(([command, handler]) => {
    bot.command(command, handler.func)
})


bot.telegram.setMyCommands(Object.entries(commands).map(([command, handler]) => {
    return {
        command: command,
        description: handler.description
    }
}))//.then(console.log).catch(console.log)


bot.launch()

let inputer = new Inputer(bot)