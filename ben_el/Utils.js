const axios = require("axios")

exports.myip = () => {
    return new Promise((resolve, reject) => {
        axios.get('https://api.ipify.org?format=json').then(res => {
            resolve(res.data.ip)
        }).catch(reject)
    })
}

exports.getArticles = () => {
    return new Promise((resolve, reject) => {
        axios.get('http://127.0.0.1:8080/feed/json').then(res => {
            resolve(res.data)
        }).catch(reject)
    })
}