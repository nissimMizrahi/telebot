var WebTorrent = require('webtorrent')
var rimraf = require("rimraf");

exports.TorrentClient = class TorrentClient{
    constructor(){
        this.client = new WebTorrent()
        this.torrents = {}
    }

    getInfo(){ 
        return Object.entries(this.torrents).map(([url, torrent]) => { return {
            url: url,
            total_downloaded: torrent.downloaded,
            down_speed: torrent.downloadSpeed,
            progress: torrent.progress,
            seconds_remaining: torrent.timeRemaining / 1000,
            ratio: torrent.ratio
        }})    
    }

    startDownloading(url, path){

        console.log('Client is downloading:', url)

        return new Promise((resolve, reject) => {
            this.client.add(url, { path: path }, (torrent) => {
                this.torrents[url] = torrent

                console.log('Client is downloading:', torrent.infoHash)

                torrent.on('download', function (bytes) {
                    console.clear();
                    let table = {
                        chunk_downloaded: bytes,
                        total_downloaded: torrent.downloaded,
                        down_speed: torrent.downloadSpeed,
                        progress: torrent.progress,
                        seconds_remaining: torrent.timeRemaining / 1000,
                        ratio: torrent.ratio
                    }
                    console.table(table)
                })
                
                torrent.on('error', reject)

                torrent.on('done', function (bytes) {
                    
                    torrent.on('upload', function (bytes) {
                        console.log("ratio", torrent.ratio)
                        if(torrent.ratio > 0.1) {
                            console.log("end")

                            setImmediate(() => {
                                torrent.pause()
                                torrent.removeAllListeners()
                                torrent.destroy(resolve)
                            })
                        }
                    })
                })
            })
        })
    }

    pauseTorrent(url){
        this.torrents[url] && this.torrents[url].pause()
    }

    resume(url){
        if(this.torrents[url] && this.torrents[url].paused) this.torrents[url].resume()
    }

    destroy(url){
        return new Promise((resolve, reject) => {
            let torrent = this.torrents[url]
            if(torrent)
            {
                torrent.pause()
                torrent.removeAllListeners()

                rimraf(torrent.path, () => null)
                torrent.destroy(resolve)
            } 
            else return reject("no such torrent") 
        })
    }

    getTorrent(url){
        return this.torrents[url] || null
    }

    release(){

        Promise.all(this.client.torrents.map(torrent => {
            return new Promise((resolve, reject) => {
                torrent.pause()
                torrent.removeAllListeners()
                torrent.destroy(resolve)
            })
        }))
        .then(() => {
            this.client.removeAllListeners()
            this.client.destroy()
        })
    }
}