const { Yts } = require("./YtsAPI")
var prompt = require('prompt-sync')();

const yifysubtitles = require('yifysubtitles');

const { TorrentClient } = require("./torrentDownloader")

let client = new TorrentClient()
let yts = new Yts()

function runGui(){
    yts.search(prompt("what movie do you want to search? ")).then(movies => {

        console.table(movies.map(movie => {
            return {
                title: movie.title,
                imdb: movie.imdb_code,
                rating: movie.rating,
            }
        }))
    
        let choice = prompt("index of movie >>> ")
        let movie = movies[choice]
    
        console.table(movie.torrents.map(torrent => {
            return {
                url: torrent.url,
                size: torrent.size
            }
        }))
    
        choice = prompt("index of torrent >>> ")
        let torrent = movie.torrents[choice]
    
        
        client.startDownloading(torrent.url, `../movies/${movie.slug}`)
            .then(() => client.release())
            .catch(console.log)
    
        yifysubtitles(movie.imdb_code, {
            path: `../movies/${movie.slug}`,
            langs: ['he'],
            format: ['srt', 'vtt'],
        })
        
    }).catch(console.log)
    
}

function runService(){
    process.on("message", (msg) => {

        let {
            torrent,
            slug,

            exit,

            info
        } = msg

        if(exit){
            client.destroy()
            process.exit(0)
        }

        else if(info){
            process.send({info: client.getInfo()})
        }
        else{
            client.startDownloading(torrent, slug)
            .then(() => process.send({done: slug}))
            .catch(console.log)
        }
        
    })
}

runService()

//https://yts.mx/torrent/download/71C36367F0416D36DC1B7712C3F662EBA65203B6