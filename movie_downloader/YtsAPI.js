let axios = require("axios")

exports.Yts = class Yts{
    constructor(){
        this.sess = axios.create({
            withCredentials: true,
            baseURL: 'https://yts.mx/api/v2'
        })
    }

    search(movie){
        return new Promise((resovle, reject) => {
            this.sess.get('/list_movies.json', {
                params: {
                    query_term: movie
                }
            })
            .then(res => {
                resovle(res.data.data.movie_count == 0 ? [] : res.data.data.movies)
            })
            .catch(reject)
        })
    }
} 